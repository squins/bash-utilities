#!/usr/bin/env bash

if [ "x${1}" == "x" ]; then
    echo Prints certificate information of certificate installed on specified hostname on port 443.
    echo Example: ./host-certificate-info.sh squins.com
    echo Missing required argument: hostname
    exit 1;
fi

domain=$1;

echo | openssl s_client -showcerts -servername ${domain} -connect ${domain}:443 2>/dev/null | openssl x509 -inform pem -noout -text